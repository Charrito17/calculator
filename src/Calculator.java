
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Calculator extends JFrame {

	private static final long serialVersionUID = -1988006666490241187L;

	char operator;
	JButton btn_Zero = new JButton("   0   ");
	JButton btn_One = new JButton("   1   ");
	JButton btn_Two = new JButton("   2   ");
	JButton btn_Three = new JButton("   3   ");
	JButton btn_Four = new JButton("   4   ");
	JButton btn_Five = new JButton("   5   ");
	JButton btn_Six = new JButton("   6   ");
	JButton btn_Seven = new JButton("   7   ");
	JButton btn_Eight = new JButton("   8   ");
	JButton btn_Nine = new JButton("   9   ");
	// Decimal
	JButton btn_Decimal = new JButton("   .   ");
	// Operators
	JButton btn_minus = new JButton("   -   ");
	JButton btn_mul = new JButton("   *   ");
	JButton btn_division = new JButton("   /   ");
	JButton btn_plus = new JButton("   +   ");
	JButton btn_equals = new JButton("   =   ");
	JTextArea txt_Area = new JTextArea(3, 5);

	// cleans up
	JButton btn_clear = new JButton(" AC ");

	public Calculator() {
		this.createGUI();
		this.showFrame();
	}

	private void createGUI() {

		add(txt_Area, BorderLayout.NORTH); // new JScrollPane(area)

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.setBackground(Color.ORANGE);
		// Button Zero
		buttonPanel.add(btn_Seven);
		btn_Seven.setBackground(Color.GREEN);
		btn_Seven.setSize(new Dimension(50, 50));
		btn_Seven.setOpaque(true);
		btn_Seven.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));

		// button eight
		buttonPanel.add(btn_Eight);
		btn_Eight.setBackground(Color.GREEN);
		btn_Eight.setSize(new Dimension(50, 50));
		btn_Eight.setOpaque(true);
		btn_Eight.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button nine
		buttonPanel.add(btn_Nine);
		btn_Nine.setBackground(Color.GREEN);
		btn_Nine.setSize(new Dimension(50, 50));
		btn_Nine.setOpaque(true);
		btn_Nine.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button Multiplication
		buttonPanel.add(btn_mul);
		btn_mul.setSize(new Dimension(1, 1));
		btn_mul.setBackground(Color.GREEN);
		btn_mul.setSize(new Dimension(50, 50));
		btn_mul.setOpaque(true);
		btn_mul.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button six
		buttonPanel.add(btn_Six);
		btn_Six.setBackground(Color.GREEN);
		btn_Six.setSize(new Dimension(50, 50));
		btn_Six.setOpaque(true);
		btn_Six.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button five
		buttonPanel.add(btn_Five);
		btn_Five.setBackground(Color.GREEN);
		btn_Five.setSize(new Dimension(50, 50));
		btn_Five.setOpaque(true);
		btn_Five.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button five
		buttonPanel.add(btn_Five);
		btn_Five.setBackground(Color.GREEN);
		btn_Five.setSize(new Dimension(50, 50));
		btn_Five.setOpaque(true);
		btn_Five.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button four
		buttonPanel.add(btn_Four);
		btn_Four.setBackground(Color.GREEN);
		btn_Four.setSize(new Dimension(50, 50));
		btn_Four.setOpaque(true);
		btn_Four.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button division
		buttonPanel.add(btn_division);
		btn_division.setBackground(Color.GREEN);
		btn_division.setSize(new Dimension(50, 50));
		btn_division.setOpaque(true);
		btn_division.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button three
		buttonPanel.add(btn_Three);
		btn_Three.setBackground(Color.GREEN);
		btn_Three.setSize(new Dimension(50, 50));
		btn_Three.setOpaque(true);
		btn_Three.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button two
		buttonPanel.add(btn_Two);
		btn_Two.setBackground(Color.GREEN);
		btn_Two.setSize(new Dimension(50, 50));
		btn_Two.setOpaque(true);
		btn_Two.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button one
		buttonPanel.add(btn_One);
		btn_One.setBackground(Color.GREEN);
		btn_One.setSize(new Dimension(50, 50));
		btn_One.setOpaque(true);
		btn_One.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button plus
		buttonPanel.add(btn_plus);
		btn_plus.setBackground(Color.GREEN);
		btn_plus.setSize(new Dimension(50, 50));
		btn_plus.setOpaque(true);
		btn_plus.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// Button Zero
		buttonPanel.add(btn_Zero);
		btn_Zero.setBackground(Color.GREEN);
		btn_Zero.setSize(new Dimension(50, 50));
		btn_Zero.setOpaque(true);
		btn_Zero.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// Decimal
		buttonPanel.add(btn_Decimal);
		btn_Decimal.setBackground(Color.GREEN);
		btn_Decimal.setSize(new Dimension(50, 50));
		btn_Decimal.setOpaque(true);
		btn_Decimal.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button equals
		buttonPanel.add(btn_equals);
		btn_equals.setBackground(Color.GREEN);
		btn_equals.setSize(new Dimension(50, 50));
		btn_equals.setOpaque(true);
		btn_equals.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button minus
		buttonPanel.add(btn_minus);
		btn_minus.setBackground(Color.GREEN);
		btn_minus.setSize(new Dimension(50, 50));
		btn_minus.setOpaque(true);
		btn_minus.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		// button clear
		buttonPanel.add(btn_clear);
		btn_clear.setBackground(Color.GREEN);
		btn_clear.setSize(new Dimension(50, 50));
		btn_clear.setOpaque(true);
		btn_clear.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 4));
		add(buttonPanel, BorderLayout.CENTER);

		txt_Area.setForeground(Color.BLUE);
		txt_Area.setBackground(Color.PINK);
		txt_Area.setLineWrap(true);
		txt_Area.setWrapStyleWord(true);
		txt_Area.setEditable(true);
		txt_Area.setFont(txt_Area.getFont().deriveFont(20f));
		// txt_Area.setFont(txt_Area.getFont().deriveFont(10));

		/*
		 * * * * * * * * * * * * * * * ACTION LISTENERS * * * * * * * * * * * * *
		 */
		btn_Decimal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append(".".toString());
			}

		});
		btn_Zero.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent f) {
				txt_Area.append("0".toString());
			}
		});

		btn_One.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent f) {
				txt_Area.append("1".toString());
			}
		});

		btn_Two.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent f) {
				txt_Area.append("2".toString());
			}
		});
		btn_Three.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("3".toString());

			}

		});
		btn_Four.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("4".toString());

			}

		});
		btn_Five.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("5".toString());
			}

		});
		btn_Six.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("6".toString());
			}

		});
		btn_Seven.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("7".toString());
			}

		});
		btn_Eight.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("8".toString());
			}

		});
		btn_Nine.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("9".toString());

			}

		});

		btn_plus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent f) {
				txt_Area.append("+".toString());
				operator = '+';
			}
		});
		btn_minus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				txt_Area.append("-".toString());
				// TODO Auto-generated method stub
				operator = '-';
			}

		});
		btn_division.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("/".toString());
				operator = '/';
			}

		});
		btn_mul.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txt_Area.append("*".toString());
				operator = '*';
			}

		});

		btn_equals.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String leftOperand = "";
				String rightOperand = "";
				String inputText;
				String outputText;

				inputText = txt_Area.getText();
				// search for the operator
				int operatorIndex = inputText.indexOf(operator);

				leftOperand = txt_Area.getText().substring(0, operatorIndex);

				if (operatorIndex == inputText.length() - 1) {
					operator = 'N';
				}

				else {
					rightOperand = txt_Area.getText().substring(operatorIndex + 1, txt_Area.getText().length());
				}

				switch (operator) {
				case '+':
					outputText = " = " + ((Double.parseDouble(leftOperand) + Double.parseDouble(rightOperand)));
					txt_Area.append(outputText);
					break;
				case '-':
					outputText = " = " + ((Double.parseDouble(leftOperand) - Double.parseDouble(rightOperand)));
					txt_Area.append(outputText);
					break;
				case '*':
					outputText = " = " + ((Double.parseDouble(leftOperand) * Double.parseDouble(rightOperand)));
					txt_Area.append(outputText);
					break;
				case '/':
					outputText = " = " + ((Double.parseDouble(leftOperand) / Double.parseDouble(rightOperand)));
					txt_Area.append(outputText);
					break;
				default:
					txt_Area.setText(" Error! ");

					break;
				}
			}
		});
		btn_clear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				txt_Area.setText(" ");
				operator = ' ';

			}

		});

	}

	private void showFrame() {

		ImageIcon img = new ImageIcon("src//Calculator.jpeg");
		this.setIconImage(img.getImage());
		this.setSize(190, 315);
		this.setTitle(" Calculator ");
		this.setResizable(false);
		this.setVisible(true);
		txt_Area.setFont(txt_Area.getFont().deriveFont(txt_Area.getFont().getSize() + 20.0f));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
